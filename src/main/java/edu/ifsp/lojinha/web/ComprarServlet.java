package edu.ifsp.lojinha.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.ifsp.lojinha.modelo.Produto;
import edu.ifsp.lojinha.persistencia.ProdutoDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/comprar.sec")
public class ComprarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ProdutoDAO produtoDao;

	@Override
	public void init() throws ServletException {
		produtoDao = new ProdutoDAO();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<Produto> carrinho = (List<Produto>)session.getAttribute("carrinho");
		if (carrinho == null) {
			carrinho = new ArrayList<Produto>();
			session.setAttribute("carrinho", carrinho);
		}
		List<Produto> produtos = produtoDao.listarTodos();
		request.setAttribute("produtos", produtos);
		
		String cmd = request.getParameter("cmd");
		if ("add".equals(cmd)) {
			String paramProduto = request.getParameter("produto");
			int id = Integer.parseInt(paramProduto);
			Produto produto = produtoDao.findById(id);
			carrinho.add(produto);			
		} else if ("esvaziar".equals(cmd)) {
			carrinho = new ArrayList<Produto>();
			session.setAttribute("carrinho", carrinho);			
		}

		/* Havia um comando válido na URL? */
		if (cmd != null) {
			response.sendRedirect("comprar.sec");
		} else {		
			RequestDispatcher rd = request.getRequestDispatcher("comprar.jsp");
			rd.forward(request, response);
		}
	}

}
